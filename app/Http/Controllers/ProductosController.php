<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;

class ProductosController extends Controller
{
    public function InicioProducto(Request $request)
    {
        $producto = Producto::all();
       return view('productos.inicio')->with('producto', $producto);
    }

    public function CrearProducto(Request $request)
    {
        $producto = Producto::all();
        return view('productos.crear')->with('producto', $producto);
    }
    
    public function GuardarProducto(Request $request){
        $this->validate($request, [
            'nombre' => 'required',
            'tipo'=> 'required',
            'estado'=> 'required',
            'precio'=> 'required'
        ]);

        $producto = new Producto;
        $producto->nombre=$request->nombre;
        $producto->tipo=$request->tipo;
        $producto->estado=$request->estado;
        $producto->precio=$request->precio;
        $producto->save();
        return redirect()->route('list.productos');
    }
}
